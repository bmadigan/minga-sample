<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">	
	<title>Minga Sample</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	
</head>
<body class="container">

	<div id="app">
		<component is="assessment-view"></component>
	</div>

	<script src="js/bundle.js" type="text/javascript"></script>

<script>document.write('<script src="http://' + (location.host || '127.0.0.1')
.split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
</body>
</html>