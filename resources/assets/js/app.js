var Vue = require('vue');
Vue.use(require('vue-resource'));

new Vue({
	el: '#app',

	components: {
		'assessment-view': require('./templates/assessment')
	}
});