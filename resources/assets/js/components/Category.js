module.exports = {
	template: require('./category.template.html'),

	props: ['when-adjusted', 'taxesOwed'],

	inherit: true,

	data: function() {
		return {
			categories: [],
			//taxesOwed: this.taxesOwed,
			category: {
				title: '',
				description: '',
				assessment_percentage: 0,
				new_assessment: 0,
			}
		};
	},

	computed: {
		originalAssessedValue: function() {
			//return this.taxesOwed ;	
		}
	},

	ready: function() {
		console.log('Owed: ' + this.taxesOwed);

		this.$http.get('/api/categories')
			.success(function(categories) {
				this.categories = categories;
			})
			.error(function() {
				console.log('Invalid categories');
			})
	},

	methods: {
		adjustAssessment: function(category) {
			this.whenAdjusted(category); // calls the parent component in assessment.js
		}
	},

	filters: {
		assessValue: function() {
			return 0; //return this.category.assessment_percentage
		}

		//$assAmount = (intval($totalTaxesOwed) * intval($c->assessment_percentage) / 100);
	}
};