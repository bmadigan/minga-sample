module.exports = {
	template: require('./chart.template.html'),

	props: [],

	data: function() {
		return {
			categories: [],
			category: {
				title: '',
				description: '',
				assessment_percentage: 0
			}
		};
	},

	ready: function() {
		this.$http.get('/api/categories')
			.success(function(categories) {
				this.categories = categories;
			})
			.error(function() {
				console.log('Invalid categories');
			})
	},

	methods: {
		update: function() {

		}
	}
};