module.exports = {

	template: require('./assessment.template.html'),

	data: function() {
		return {
			budget: {
				title: '',
				apv: 0,
				apap: 0
			}
		};
	},

	ready: function() {
		//this.children = this._transCpnts;

		this.$http.get('/api/budgets/latest')
			.success(function(budget) {
				this.budget.title = budget.title;
				this.budget.apv = budget.average_property_value;
				this.budget.apap = budget.assessment_percentage;
			})
			.error(function() {
				this.budget.title = 'Invalid Budget'
			})
	},

	computed: {
		taxesOwed: function() {
			return (this.budget.apv * this.budget.apap) / 100;	
		}
	},

	methods: {
		updateChart: function(category) {
			// need to update the chart
			var original = this.taxesOwed * (category.assessment_percentage / 100);
			console.log('Orig Val: ' + original);

			var dummy = (original * (category.new_assessment / 100) + original);

			console.log('New Val: ' + dummy);
		}
	},

	components: {
		category: require('../components/Category'),
		chart: require('../components/Chart'),
	}
}