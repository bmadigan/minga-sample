<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('budget_id');
            $table->string('title');
            $table->string('description')->nullable();
            $table->tinyInteger('assessment_percentage');
            $table->tinyInteger('low_threshold')->default(0);
            $table->tinyInteger('high_threshold')->default(0);
            $table->string('low_desc')->nullable();
            $table->string('high_desc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
