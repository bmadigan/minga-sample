<?php

use App\Budget;
use App\Category;

get('/', function() {
	return view('home');
});

// API

get('api/budgets/latest', function() {
	return Budget::find(1);
});

get('api/categories', function() {
	return Category::all();
});
